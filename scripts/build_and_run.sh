#!/bin/bash -e

cd ..

cd scripts

source /opt/Xilinx/Vivado/2018.2/settings64.sh

vivado -nolog -nojournal -mode tcl ../vivado_project/*.xpr -source ./vivado.tcl

# Set up the SDK Workspace
xsct ./sdk_setup.tcl

xsct ./sdk_build.tcl

source run_test.sh
