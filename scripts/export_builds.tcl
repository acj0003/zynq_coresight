# This TCL script will copy the bitstream to the output folder. 
set base_dir ..
set sdk_dir $base_dir/sources/sdk
set build_dir $base_dir/build


open_run impl_1

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
write_bitstream -force -verbose $build_dir/zynq_coresight.bit

write_hwdef -force  -file $sdk_dir/zynq_coresight.hdf

