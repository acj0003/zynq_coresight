#!/bin/sh -e 

echo "
  _____                    ____                   _       _     _     _____        _    
 |__  _   _ _ __   __ _   / ___|___  _ __ ___ ___(_) __ _| |__ | |_  |_   ____ ___| |_  
   / | | | | '_ \ / _` | | |   / _ \| '__/ _ / __| |/ _` | '_ \| __|   | |/ _ / __| __| 
  / /| |_| | | | | (_| | | |__| (_) | | |  __\__ | | (_| | | | | |_    | |  __\__ | |_  
 /____\__, |_| |_|\__, |  \____\___/|_|  \___|___|_|\__, |_| |_|\__|   |_|\___|___/\__| 
      |___/          |_|                            |___/                               

"


# set up the enviroments
source /opt/Xilinx/Vivado/2018.2/settings64.sh

base_dir=..
source_dir=$base_dir/sources/sdk
results_dir=$base_dir/results
tools_dir=$base_dir/tools

udp_port=12345

# validate user input
if [ -z "$1" ]
then
    echo "Iterations not specified. Defaulting to 10 iterations..."
    sleep 2
    count=10
else
    count=$1
fi


binary_file="$results_dir/tpiu_$count.bin"
text_file="$results_dir/tpiu_$count.txt"

# replace the number fo times to run in the source code
sed -i "s/.*const int times_to_run.*/const int times_to_run = $count;/" $source_dir/coresight/src/main.c
# rebuild the source code
xsct sdk_build.tcl

# gather the code for the matrix assembly. 
arm-none-eabi-objdump -dS  -j .matrix $source_dir/coresight/Debug/coresight.elf > $results_dir/martix_assembly.txt

#xsct sdk_coresight_etb.tcl

echo "running netcat"
nc -l -u $udp_port > $binary_file &
ncpid=$!

# execute the programs on the Zynq device
xsct sdk_coresight_eth.tcl

#stop netcat
kill $ncpid

echo "running ptm"
$tools_dir/ptm2human/ptm2human -r -i $binary_file -c 4 > $text_file
$tools_dir/ptm2human/ptm2human -r -d -i $binary_file -c 4 > $results_dir/tpiu_$count_debug.txt 2> $results_dir/tpiu_$count_log.txt
$tools_dir/ptm2human/ptm2human -r -d -i $binary_file -c 4 &> $results_dir/tpiu_$count_combined.txt

echo "Number of iterations expected"
echo $count
echo "Number of iterations recorded"
grep -o 'instruction addr at 0xffff0000' $text_file | wc -l
printf "Recorded in %s\n"  $text_file
