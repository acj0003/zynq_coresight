setws ../sources/sdk/


projects -type app -clean -name free_ip_coresight
projects -type app -clean -name coresight
projects -clean -type bsp -name coresight_bsp
projects -clean -type bsp -name free_ip_coresight_bsp

# Build the BSPs first
projects -build -type bsp -name coresight_bsp
projects -build -type bsp -name free_ip_coresight_bsp
# Now build the apps
projects -type app -build -name free_ip_coresight
projects -type app -build -name coresight

