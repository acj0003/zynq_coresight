set base_dir ..
set source_dir $base_dir/sources/sdk
set results_dir $base_dir/results
set build_dir $base_dir/build

setws $source_dir

#cd $source_dir

projects -clean
projects -build

#connect to remote hw_server by specifying its url.
#If the hardware is connected to a local machine,-url option and the <url>
#are not needed. connect command returns the channel ID of the connection
connect
# List available targets and select a target through its id.
#The targets are assigned IDs as they are discovered on the Jtag chain,
#so the IDs can change from session to session.
#For non-interactive usage, -filter option can be used to select a target,
#instead of selecting the target through its ID
targets -set -filter {name =~ "ARM* #0"}

# Reset the system before initializing the PS and configuring the FPGA
rst
# Info messages are displayed when the status of a core changes

# Configure the FPGA. When the active target is not a FPGA device,
#the first FPGA device is configured
fpga $build_dir/zynq_coresight.bit

# Run loadhw command to make the debugger aware of the processor cores’ memory map
loadhw $source_dir/hw_platform/system.hdf

# Source the ps7_init.tcl script and run ps7_init and ps7_post_config commands
source $source_dir/hw_platform/ps7_init.tcl

ps7_init

ps7_post_config

# Download the application program
dow $source_dir/coresight/Debug/coresight.elf

# Start processor 1
targets -set -filter {name =~ "ARM* #0"}
 
# Set a breakpoint at main()
bpadd -addr &main
con -block 

bpadd -file /coresight/src/main.c -line 149

# Resume the processor core
con -block -timeout 10

#dump the contents pulled from the ETB buffer 
mrd -bin -file $results_dir/etb.bin 0x18c0 1024


stop
bt
exec sleep 2
unloadhw
