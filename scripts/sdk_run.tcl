# This will download the application the board
#
# NOTE: To get the UART to display correctly, you must
# start the xsct without arguements, then
# enter the following command "source sdk_run.tcl"

setws ../sources/sdk/

set ws ../sources/sdk/
#connect to remote hw_server by specifying its url.
#If the hardware is connected to a local machine,-url option and the <url>
#are not needed. connect command returns the channel ID of the connection
connect -host 10.16.0.82 -port 3121
# List available targets and select a target through its id.
#The targets are assigned IDs as they are discovered on the Jtag chain,
#so the IDs can change from session to session.
#For non-interactive usage, -filter option can be used to select a target,
#instead of selecting the target through its ID
targets -set -filter {name =~ "ARM* #0"}

# Reset the system before initializing the PS and configuring the FPGA
rst
# Info messages are displayed when the status of a core changes

# Configure the FPGA. When the active target is not a FPGA device,
#the first FPGA device is configured
fpga ../build/longbowmav_wrapper.bit

# Run loadhw command to make the debugger aware of the processor cores’ memory map
loadhw $ws/longbowmav_wrapper_hw_platform_0/system.hdf

# Source the ps7_init.tcl script and run ps7_init and ps7_post_config commands
source $ws/longbowmav_wrapper_hw_platform_0/ps7_init.tcl
ps7_init
ps7_post_config

# Download the application program
dow $ws/lbmav_app/Debug/lbmav_app.elf

# Start the application
con

# Open the JTAG UART
jtagterminal


