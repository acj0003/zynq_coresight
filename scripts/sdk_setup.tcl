# run this script from Xilinx 'xsct' command line tool
# Refer UG1208 Software Command-Line Toop Reference Guide
# https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_2/ug1208-xsct-reference-guide.pdf
set source_dir ../sources/sdk
########## Workspace setup
setws $source_dir/
importprojects $source_dir/

######## Open the hw design
#createhw -name hw_platform -hwspec $source_dir/zynq_coresight.hdf
updatehw -hw hw_platform -newhwspec $source_dir/zynq_coresight.hdf
openhw hw_platform


######## Application BSP
# Regenerate the BSP 
openbsp ../sources/sdk/free_ip_coresight_bsp/system.mss
regenbsp -bsp free_ip_coresight_bsp
closebsp ../sources/sdk/free_ip_coresight_bsp/system.mss

# Regenerate the BSP 
openbsp ../sources/sdk/coresight_bsp/system.mss
regenbsp -bsp coresight_bsp
closebsp ../sources/sdk/coresight_bsp/system.mss

closehw ../sources/sdk/hw_platform/system.hdf
