## This script will reset and build the Vivado Project. It will them export the
#  hardware definition to the XSDK workspace.


set script_dir  [file dirname  [info script ]]

set eiu_dir $script_dir/..
cd $script_dir

set coresight_bd ../sources/bd/system/system.bd

# clear out the cache if any exsists.
set_param project.defaultIPCacheSetting none
config_ip_cache -clear_output_repo

# use 8 threads. gouge it driver!
# TODO: use this code to get number of cores:
# 	https://www.xilinx.com/support/answers/66966.html
set_param general.maxThreads 8

# reset the block design
reset_target all [get_files  $coresight_bd]
export_ip_user_files -of_objects  [get_files  $coresight_bd] -sync -no_script -force -quiet

# convert the block design to synthesis globally, versus per block design, or per ip
# in the developer flow we wouldn't want this
# but since we are building from scratch we might as well. 
update_compile_order -fileset sources_1
set_property synth_checkpoint_mode None [get_files  $coresight_bd]
generate_target all [get_files  $coresight_bd]


# reset the synthesis runs
reset_run synth_1
# synthesize
launch_runs synth_1
wait_on_run synth_1

# run implmentations, to include writing the bitstream
# TODO: use this code to get number of cores:
# 	https://www.xilinx.com/support/answers/66966.html
launch_runs impl_1 -jobs 16
wait_on_run impl_1

# copy the bitstream to the synthesis folder. 
source export_builds.tcl

exit


