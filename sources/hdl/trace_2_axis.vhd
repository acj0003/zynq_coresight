----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 02/24/2019 08:42:17 PM
-- Design Name:
-- Module Name: trace_2_axis - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trace_2_axis is
generic(
constant max_count : natural := 1024
);
  port (
    trace_data     : in  std_logic_vector (31 downto 0);
    m_axis_aclk    : in  std_logic;
    m_axis_aresetn : in  std_logic;
    m_axis_tdata   : out std_logic_vector (31 downto 0);
    m_axis_tvalid  : out std_logic;
    m_axis_tlast   : out std_logic;
    m_axis_tready  : in  std_logic;
    m_axis_tuser   : out std_logic;
    trace_ctl      : in  std_logic);
end trace_2_axis;

architecture Behavioral of trace_2_axis is

  
  signal word_count  : natural := 0;

begin


  process (m_axis_aclk)
  begin
    if rising_edge(m_axis_aclk) then
      m_axis_tdata <= trace_data;
      m_axis_tlast <= '0';
      if m_axis_aresetn = '0' then
        m_axis_tvalid <= '0';
        word_count    <= 0;
      else
        m_axis_tvalid <= not trace_ctl;
          if(word_count = max_count - 1) then
          m_axis_tlast <= '1';
          end if;
        if(trace_ctl = '0') then
          if(word_count = max_count - 1) then
            word_count   <= 0;
            
          else
            word_count <= word_count + 1;
          end if;
        end if;
      end if;
    end if;
  end process;



end Behavioral;
