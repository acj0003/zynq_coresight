################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/etb.c \
../src/funnel.c \
../src/itm.c \
../src/main.c \
../src/platform.c \
../src/ptm.c \
../src/tpiu.c \
../src/ubenchmark.c 

OBJS += \
./src/etb.o \
./src/funnel.o \
./src/itm.o \
./src/main.o \
./src/platform.o \
./src/ptm.o \
./src/tpiu.o \
./src/ubenchmark.o 

C_DEPS += \
./src/etb.d \
./src/funnel.d \
./src/itm.d \
./src/main.d \
./src/platform.d \
./src/ptm.d \
./src/tpiu.d \
./src/ubenchmark.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -Wstrict-prototypes -Wmissing-prototypes -Wshadow -Wconversion -I../../coresight_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


