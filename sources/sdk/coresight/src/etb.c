/*
 * etb.c
 *
 *  Created on: Mar 3, 2019
 *      Author: ashton
 */

#include "etb.h"

#include <assert.h>




extern unsigned int etb_stored[ETB_SIZE_WORDS];

int etb_setup_default(struct etb_config * config) {
	/* unlock the module */

	config->lar = UNLOCK_KEY;
	config->ctl = 0; // disable the trace capture
	config->trg = config->rdp / 2; // set trigger counter to half the trace ram depth

	assert(config->lsr && IS_LOCKED);

	// reset the write pointer and erase the data
	config->rwp = 0;
	for (int i = 0; i < config->rdp; i++) {
		config->rwd = i;
	}

	// reset the pointers
	config->rwp = 0;
	config->rrp = 0;

	// read the read data (just for debugging)
	for (int i = 0; i < config->rdp; i++) {
		i = config->rrd;
	}

	// reset the write pointer and flush the write data
	config->rwp = 0;
	for (int i = 0; i < config->rdp; i++) {
		config->rwd = 0;
	}

	// reset the write pointer
	config->rwp = 0;
	config->rrp = 0;


	// set trigger counter to half the trace ram depth
	config->trg = config->rdp / 2;

	 // indicate a trigger on trigger event
	config->ffcr = (1 << 9);
	//enable normal formatting of the data.
	//config->ffcr |= (1 << 0);

	// enable tracing
	config->ctl = 1;

	// lock the registers
	config->lar = 0xCAFEBABE;

}

unsigned int etb_read(struct etb_config * config) {
	// disable the tracing
	config->ctl = 0;
	// unlock the registers
	config->lar = UNLOCK_KEY;
	// disable the tracing
	config->ctl = 0;
	// reset the read pointer
	config->rrp = 0;

	// The buffer has overflowed!
	if (0UL != (0x01UL & config->sts)){
		config->sts = config->sts;
	}

	// read out the trace data
	unsigned int amount_to_read = config->rwp;
	for (int i = 0; i < amount_to_read; i++)
		etb_stored[i] = config->rrd;

	// reset the etb for more capturing
	etb_setup_default(config);

	return 0;

}
