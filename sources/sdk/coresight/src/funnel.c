/*
 * funnel.c
 *
 *  Created on: Mar 1, 2019
 *      Author: ashton
 */

#include "funnel.h"
#include <assert.h>

int coresight_funnel_init(uint32_t funnel_base_addr) {

	/* unlock the module */
	uint32_t volatile * const lar = (uint32_t *) (funnel_base_addr + LAR);
	*lar = UNLOCK_KEY;


	uint32_t volatile * const lsr = (uint32_t *) (funnel_base_addr + LSR);
	assert( *(lsr) && IS_LOCKED );

	uint32_t volatile * working_reg = (uint32_t *) (funnel_base_addr + PRICONTROL);


	/* don't change PRICONTROl */
	*working_reg = *working_reg;

	/* Enable all of the inputs */
	working_reg = (uint32_t *) (funnel_base_addr + CONTROL);
    *working_reg = 0x01;
    assert( *working_reg && 0x01);



	/* lock the module */
	*lar = 0;

}
