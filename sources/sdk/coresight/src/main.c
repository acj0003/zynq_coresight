/**
   Coresight Synethetic Benchmark and Instrumentation Application

   Instruction:
   Write a C program that multiplies two squared matrices A and B of size N (NxN). The matrices consist of double-
   precision elements. The program should accept the matrix size, N, as a parameter; initialize the matrices using a
   random number generator; and find the resulting matrix C=A*B. The resulting matrix should be written to a binary
   file.

   Copyright (C) 2017  Ashton Johnson
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#include "xil_exception.h"

#include "etm.h"
#include "tpiu.h"
#include "funnel.h"
#include "etb.h"
#include "itm.h"
#include "xil_printf.h"

#define USE_ETB

unsigned int __attribute((section(".etb_data"))) etb_stored[ETB_SIZE_WORDS] = {
  0 };

tpiu_config_t *tpiu_conf;
struct etm_map *etm_config;
struct etb_config *etb;

/* This will get automatically updated in the script */
const int times_to_run = 10;

const void * __matrix_start; /* start address of the .matrix section. __matrix_start is defined in linker scripts */
const void * __matrix_end; /* end address of the .matrix section. __matrix_end is defined in linker scripts */

const void * invalid_loc = 0x40000000;

void setThreadID(int id);
void setContextID(int id);



int __attribute((section(".matrix"))) microBenchMark_factorial(int input);
int __attribute((section(".matrix"))) microBenchMark_abs(int input);

void __attribute((section(".alt"))) microBenchMark_swi(int input) {
  asm("swi 0");
}

void __attribute((section(".alt"))) microBenchMark_pfa(int input) {
  void (*pf)(int) = invalid_loc;
  (pf)(5);
}

void __attribute((section(".alt"))) microBenchMark_swiHandler(void *Data) {
  static int count = 0;
  count++;
}

void __attribute((section(".alt"))) microBenchMark_pfaHandler(void *Data) {
  static int count = 0;
#ifdef USE_ETB
  etb_read(etb);
#endif
  count++;
}

void __attribute((section(".alt"))) microBenchMark_dbg(int input) {

  if (0 < input) {
    asm("bkpt #1000");

  } else {

  }
}

int main(int argc, char *argv[]) {

  Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_SWI_INT,
			       microBenchMark_swiHandler, 0);
  Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_PREFETCH_ABORT_INT,
			       microBenchMark_pfaHandler, 0);

  etm_config = ZYNQ_ETM_CPU0;
  tpiu_conf = TPIU_BASE_ADDR;

#ifdef USE_ETB
  etb = ETB_ZYNQ_ADDR;
  etb_setup_default(etb);
#endif

  tpiu_setup_default(tpiu_conf);

  coresight_funnel_init(FUNNEL_BASE_ADDR);
  etm_map_default(etm_config, (uint32_t *) &__matrix_start,
		  (uint32_t *) &__matrix_end);
  //itm_init();
  int result;
  int delay = 5;

#ifdef USE_ETB
  //for (loop_idx = -150; loop_idx < 1; loop_idx++) { // valid for ETB
  for (int i = 0; i < times_to_run; i++) { // valid for ETB

    //for (int i = -2; i < 2; i++) {
#else
    //for (int i = -197; i < 3000; i++) { // think this is the TPIU limit

    //for (int i = -10000; i < 9999; i++) {
    for (int i = 0; i < times_to_run; i++) {
#endif

      setContextID(i);
      //microBenchMark_abs(i);
      microBenchMark_factorial(i);

    }

#ifdef USE_ETB
    etb_read(etb);
#endif

    return 0;
  }

  
  void setThreadID(int id) {
    register int R0 __asm("r0");
    R0 = id;
    asm("mcr	p15, 0, r0, c13, c0, 4");
  }

  void setContextID(int id) {
    register int R0 __asm("r0");
    R0 = id + 0x50;
    asm("mcr	p15, 0, r0, c13, c0, 1");
  }

 
  int __attribute((section(".matrix"))) microBenchMark_abs(int input) {
 
    int output;
    if (0 == input % 2) {
      output = (0 - input);
    } else {
      output = input;
    }

    return output;
  }

  int __attribute((section(".matrix"))) microBenchMark_factorial(int input) {
    int output;
    for (int i = 0; i < input ; i++){
      output += i;

      if (0 == input % 2) {
	output = (0 - input);
      } else {
	output = input;
      }
    
    }
    return output;
  }
