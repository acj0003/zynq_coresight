/*
 * ptm.c
 *
 *  Created on: Mar 1, 2019
 *      Author: ashton
 */

#include "etm.h"
#include "coresight-etm.h"
#include <xil_io.h>

void etm_reset(struct etm_map *config) {
	config->lar = UNLOCK_KEY;

	config->etmcr &= !BIT(10); // enable the programming bit.

	while (!(config->etmsr & BIT(1))) {
		// waiting to indicate the it is in programming mode
	}

	config->etmcr &= !BIT(0);  // shut down the unit.

	config->etmcr |= BIT(12);  // enable cycle accurate

	config->etmcr |= BIT(8);  // enable branch output

	config->etmcr |= BIT(15);  // context ID size
	config->etmcr |= BIT(14);  // context ID size

	config->etmcr &= !BIT(28);  // enable timestamp

	config->etmcr &= !BIT(29);  // enable return stack

	config->etm_trigger = ETM_HARD_WIRE_RES_A; //etm trigger
	config->etmeevr = ETM_HARD_WIRE_RES_A; // trace enable event.

}

void etm_flush(struct etm_map *config) {

	config->lar = UNLOCK_KEY;
	config->etmcr |= BIT(10); // enable the programming bit.
	config->etmcr &= ~BIT(10); // enable the programming bit.
	config->lar = 0;

}

void etm_enable(struct etm_map *config) {

	etm_reset(config);

	config->etmcr &= !BIT(10);

	config->lar = 0;

}

void etm_set_default(struct etm_config *config) {
	int i;

	if (!config)
		return;

	/*
	 * Taken verbatim from the TRM:
	 *
	 * To trace all memory:
	 *  set bit [24] in register 0x009, the ETMTECR1, to 1
	 *  set all other bits in register 0x009, the ETMTECR1, to 0
	 *  set all bits in register 0x007, the ETMTECR2, to 0
	 *  set register 0x008, the ETMTEEVR, to 0x6F (TRUE).
	 */
	config->enable_ctrl1 = BIT(24);
	config->enable_ctrl2 = 0x0;
	config->enable_event = ETM_HARD_WIRE_RES_A;

	config->trigger_event = ETM_DEFAULT_EVENT_VAL;
	config->enable_event = ETM_HARD_WIRE_RES_A;

	config->seq_12_event = ETM_DEFAULT_EVENT_VAL;
	config->seq_21_event = ETM_DEFAULT_EVENT_VAL;
	config->seq_23_event = ETM_DEFAULT_EVENT_VAL;
	config->seq_31_event = ETM_DEFAULT_EVENT_VAL;
	config->seq_32_event = ETM_DEFAULT_EVENT_VAL;
	config->seq_13_event = ETM_DEFAULT_EVENT_VAL;
	//config->timestamp_event = ETM_DEFAULT_EVENT_VAL;

	for (i = 0; i < ETM_MAX_CNTR; i++) {
		config->cntr_rld_val[i] = 0x0;
		config->cntr_event[i] = ETM_DEFAULT_EVENT_VAL;
		config->cntr_rld_event[i] = ETM_DEFAULT_EVENT_VAL;
		config->cntr_val[i] = 0x0;
	}

	config->seq_curr_state = 0x0;
	config->ctxid_idx = 0x0;
	for (i = 0; i < ETM_MAX_CTXID_CMP; i++)
		config->ctxid_pid[i] = 0x0;

	config->ctxid_mask = 0x0;
	/* Setting default to 1024 as per TRM recommendation */
	config->sync_freq = 0x400;
}

void etm_map_default(struct etm_map *config, uint32_t *start_addr,
		uint32_t *stop_addr) {
	int i;

	config->lar = UNLOCK_KEY;
	/*
	 * Taken verbatim from the TRM:
	 *
	 * To trace all memory:
	 *  set bit [24] in register 0x009, the ETMTECR1, to 1
	 *  set all other bits in register 0x009, the ETMTECR1, to 0
	 *  set all bits in register 0x007, the ETMTECR2, to 0
	 *  set register 0x008, the ETMTEEVR, to 0x6F (TRUE).
	 */
	config->lar = UNLOCK_KEY;

	config->etmcr |= BIT(10); // enable the programming bit.

	while (!(config->etmsr & BIT(1))) {
		// waiting to indicate the it is in programming mode
	}

	config->etmcr |= BIT(0);  // shut down the unit.
	config->etmcr &= ~BIT(0);  // disable shutdown.
	config->etmcr |= BIT(10); // enable the programming bit.
	while (!(config->etmsr & BIT(1))) {
		// waiting to indicate the it is in programming mode
	}



	config->etmtecr1 = 0x0;
	config->etmcr &= ~BIT(12);  // enable cycle accurate
	config->etmcr |= BIT(8);  // enable branch output

	/* Set Context ID */
	config->etmcr |= BIT(15);  // context id size
	config->etmcr |= BIT(14);  // context id size
	config->etmcr &= ~BIT(28);  // enable timestamp
	config->etmcr &= ~BIT(29);  // enable return stack

	config->etmeevr = ETM_HARD_WIRE_RES_A;
	config->etm_trigger = ETM_HARD_WIRE_RES_A;

	config->etmsq12evr = ETM_HARD_WIRE_RES_A;
	config->etmsq21evr = ETM_HARD_WIRE_RES_A;
	config->etmsq23evr = ETM_HARD_WIRE_RES_A;
	config->etmsq31evr = ETM_HARD_WIRE_RES_A;
	config->etmsq32evr = ETM_HARD_WIRE_RES_A;
	config->etmsq31evr = ETM_HARD_WIRE_RES_A;
	config->etmsq13evr = ETM_HARD_WIRE_RES_A;
	config->etmtsevr = 0; //ETM_HARD_WIRE_RES_A;
	config->etmauxcr = 0x8; //ETM_HARD_WIRE_RES_A;

	for (i = 0; i < ETM_MAX_CNTR; i++) {
		config->etmcntrldvr[i] = 0x0;
		config->etmcntenr[i] = ETM_HARD_WIRE_RES_A;
		config->etmcntrldevr[i] = ETM_HARD_WIRE_RES_A;
		config->etmcntvr[i] = 0x0;
	}


	config->etmtecr1 =  (1 << 25) | (1<<0);
	config->etmeevr =  0x10; // not sure if we need this
	config->etmeevr = ETM_HARD_WIRE_RES_A;
	config->etm_trigger = ETM_DEFAULT_EVENT_VAL;

	config->etmsscr = (1<<0) | (2 << 16); //set addresscomp 1 as start and 2 as stop

	config->etmavcr[0] = (uint32_t) start_addr;

	config->etmavcr[1] = (uint32_t) stop_addr;

	config->etmsqr = 0x0;
	//config->ctxid_idx = 0x0;
	for (i = 0; i < ETM_MAX_CTXID_CMP; i++)
		config->etmcidcvr1 = 0x0;

	config->etmextoutevr[0] = ETM_HARD_WIRE_RES_A;
	config->etmextoutevr[1] = ETM_HARD_WIRE_RES_A;

	config->etmtraceidr = 0x1; //set the trace id to 1

	config->etmcidcmr = 0x0;
	/* Setting default to 1024 as per TRM recommendation */
	config->etmsyncfr = 0x100;


	config->etmcr &= ~BIT(10); // enable the programming bit.

	config->lar = 0x0;
}


void etm_enable_range(struct etm_map *config, uint32_t *start_addr,
		uint32_t *stop_addr) {

	config->lar = UNLOCK_KEY;
	config->etmcr |= BIT(10); // enable the programming bit.

	config->etmtecr1 =  (1 << 25) | (1<<0);
	config->etmeevr =  0x10;
	config->etm_trigger = ETM_DEFAULT_EVENT_VAL;

	config->etmsscr = (1<<0) | (2 << 16); //set addresscomp 1 as start and 2 as stop

	config->etmavcr[0] = (uint32_t) start_addr;

	config->etmavcr[1] = (uint32_t) stop_addr;

	config->etmcr &= ~(BIT(10)); // enable the programming bit.

}
