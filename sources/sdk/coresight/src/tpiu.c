/*
 * tpiu.c
 *
 *  Created on: Feb 24, 2019
 *      Author: ashton
 */

#include "tpiu.h"

int tpiu_setup_default(tpiu_config_t * config) {

	/* unlock the tpiu */
	config->lar = 0xC5ACCE55;

	config->trigcount = 0; //FF; // no of words to output before trigger inserted
	config->trigmult = 0; // multiplier of trigcount

	//config->ffcr = TPIU_FFCR_TRIG_EVT; //indicates a trigger on trigger event
	config->ffcr |= TPIU_FFCR_F_ON_TRIG; //flush on trigger event
//		config->ffcr |= TPIU_FFCR_ENFCONT; //enable cont formatting of the data.
		//config->ffcr |= TPIU_FFCR_ENFTC; //enable normal formatting of the data.

	/* lock the tpiu back */
	config->lar = 0;
}

int tpiu_set_trace_clk_ext(uint32_t tpiu_base_addr) {

	/* unlock the clock controller */
	uint32_t volatile * const slck_unlock = (uint32_t *) 0xF8000008;
	*slck_unlock = 0xDF0D;

	/* configure the bebug clock */
	uint32_t volatile * const DBG_CLK_CTRL = (uint32_t *) (0xF8000164);
	*DBG_CLK_CTRL = (1 << 6) | (*DBG_CLK_CTRL); // select EMIU trace clock

	/* clock back the clock controller */
	uint32_t volatile * const slck_lock = (uint32_t *) 0xF8000004;
	*slck_unlock = 0x767B;

}

int tpiu_test_configure(tpiu_config_t * config, tpiu_test_pattern_e pattern) {

	/* unlock the tpiu */
	config->lar = 0xC5ACCE55;

	config->testrepeatcount = 31UL;

	config->currenttest = pattern;
	config->currenttest |= TPIU_TIMED_MODE;

	//config->ffcr |= TPIU_FFCR_F_ON_MAN;
	/* lock the tpiu back */
	config->lar = 0;

}
