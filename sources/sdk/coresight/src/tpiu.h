/*
 * tpiu.h
 *
 *  Created on: Feb 20, 2019
 *      Author: ashton
 */

#ifndef SRC_TPIU_H_
#define SRC_TPIU_H_

#include <stdint.h>

#define TPIU_BASE_ADDR 0xF8803000

/** Supported Port Size Register */
#define TPIU_SUPPSIZE 0x00000000
/** Current Port Size Register */
#define TPIU_CURRENTSIZE 0x00000004 
/** Supported Trigger Modes Register */
#define TPIU_SUPPTRIGMODE 0x00000100  
/** Trigger Counter Register */
#define TPIU_TRIGCOUNT 0x00000104  
/** Trigger Multiplier Register */
#define TPIU_TRIGMULT 0x00000108  
/** Supported Test Patterns/Modes Register */
#define TPIU_SUPPTEST 0x00000200  
/** Current Test Patterns/Modes Register */
#define TPIU_CURRENTTEST 0x00000204

#define TPIU_CONTINUOUS_MODE (1UL<<17)
#define TPIU_TIMED_MODE (1UL<<16)

#define TPIU_FF_00_PATTERN_MODE (1UL<<3)
#define TPIU_AA_55_PATTERN_MODE (1UL<<2)
#define TPIU_WALKING_ZEROS_MODE (1UL<<1)
#define TPIU_WALKING_ONES_MODE (1UL<<0)

typedef enum { WALKING_ONES = (1UL<<0),
				WALKING_ZEROS = (1UL<<1),
				AA_55_PATTERN = (1UL<<2),
				FF_00_PATTERN = (1UL<<3)
} tpiu_test_pattern_e;


#define TPIU_FFCR_TRIG_EVT (1UL << 9)
#define TPIU_FFCR_F_ON_MAN (1UL << 6)
#define TPIU_FFCR_F_ON_TRIG (1UL << 5)
#define TPIU_FFCR_ENFCONT (1UL << 1)
#define TPIU_FFCR_ENFTC (1UL << 0)

/** TPIU Test Pattern Repeat Counter Register */
#define TPIU_TESTREPEATCOUNT 0x00000208  
/** Formatter and Flush Status Register */
#define TPIU_FFSR 0x00000300  
/** Formatter and Flush Control Register */
#define TPIU_FFCR 0x00000304  
/** Formatter Synchronization Counter Register */
#define TPIU_FORMATSYNCCOUNT 0x00000308  
/** EXTCTL In Port */
#define TPIU_EXTCTLIn 0x00000400  
/** EXTCTL Out Port */
#define TPIU_EXTCTLOut 0x00000404  
/** Integration Test Trigger In and Flush In Acknowledge Register */
#define TPIU_ITTRFLINACK 0x00000EE4  
/** Integration Test Trigger In and Flush In Register */
#define TPIU_ITTRFLIN 0x00000EE8  
/** Integration Test ATB Data Register 0 */
#define TPIU_ITATBDATA0 0x00000EEC  
/** Integration Test ATB Control Register 2 */
#define TPIU_ITATBCTR2 0x00000EF0  
/** Integration Test ATB Control Register 1 */
#define TPIU_ITATBCTR1 0x00000EF4  
/** Integration Test ATB Control Register 0 */
#define TPIU_ITATBCTR0 0x00000EF8  
/** Integration Mode Control Register */
#define TPIU_IMCR 0x00000F00  
/** Claim Tag Set Register */
#define TPIU_CTSR 0x00000FA0  
/** Claim Tag Clear Register */
#define TPIU_CTCR 0x00000FA4  
/** Lock Access Register */
#define TPIU_LAR 0x00000FB0  
/** Lock Status Register */
#define TPIU_LSR 0x00000FB4  
/** Authentication Status Register */
#define TPIU_ASR 0x00000FB8  
/** Device ID */
#define TPIU_DEVID 0x00000FC8  
/** Device Type Identifier Register */
#define TPIU_DTIR 0x00000FCC  
/** Peripheral ID4 */
#define TPIU_PERIPHID4 0x00000FD0  
/** Peripheral ID5 */
#define TPIU_PERIPHID5 0x00000FD4  
/** Peripheral ID6 */
#define TPIU_PERIPHID6 0x00000FD8  
/** Peripheral ID7 */
#define TPIU_PERIPHID7 0x00000FDC  
/** Peripheral ID0 */
#define TPIU_PERIPHID0 0x00000FE0  
/** Peripheral ID1 */
#define TPIU_PERIPHID1 0x00000FE4  
/** Peripheral ID2 */
#define TPIU_PERIPHID2 0x00000FE8  
/** Peripheral ID3 */
#define TPIU_PERIPHID3 0x00000FEC  
/** Component ID0 */
#define TPIU_COMPID0 0x00000FF0  
/** Component ID1 */
#define TPIU_COMPID1 0x00000FF4  
/** Component ID2 */
#define TPIU_COMPID2 0x00000FF8  
/** Component ID3 */
#define TPIU_COMPID3 0x00000FFC  

typedef uint32_t u32;
typedef uint8_t u8;

typedef struct  {

u32 suppsize;
u32 currentsize;
u8 res0[(0x100-0x004-0x04)];
u32 supptrigmode;
u32 trigcount;
u32 trigmult;
u8 res1[(0x200-0x108-0x04)];
u32 supptest;
u32 currenttest;
u32 testrepeatcount;
u8 res2[(0x300-0x208-0x04)];
u32 ffsr;
u32 ffcr;
u32 formatsyncount;
u8 res3[(0x400-0x308-0x04)];
u32 extctlin;
u32 extcltout;
u8 res4[(0xee4-0x404-0x04)];
u32 ittrflinack;
u32 ittrflin;
u32 itatbdata0;
u32 itatdctr2;
u32 itatbctr1;
u32 itatbctr0;
u8 res5[(0xf00 - 0xef8 - 0x04)];
u32 imcr;
u8 res6[(0xfa0 - 0xf00 - 0x04)];
u32 ctsr;
u32 ctcr;
u8 res7[(0xfb0 - 0xfa4 - 0x04)];
u32 lar;
u32 lsr;


} tpiu_config_t;



int tpiu_test_configure(tpiu_config_t *, tpiu_test_pattern_e);
int tpiu_set_trace_clk_ext(uint32_t tpiu_base_addr);
int tpiu_setup_default(tpiu_config_t * config);


#endif /* SRC_TPIU_H_ */
