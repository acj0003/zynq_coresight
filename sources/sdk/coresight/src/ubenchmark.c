





int __attribute((section(".matrix_last"))) abs(int input){

  asm("DBG #1");
  int output; 
  if ( 0 > input ) {
    asm("DBG #2");
    output = input; 
  } else{
    asm("DBG #3"); //does the comment follow
    output =  0 - input ;
  }
  asm("DBG #4");
  return output;
}
