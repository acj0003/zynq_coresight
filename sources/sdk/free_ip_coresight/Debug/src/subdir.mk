################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/iic_phyreset.c \
../src/main.c \
../src/trace_fifo.c \
../src/trace_udp.c 

OBJS += \
./src/iic_phyreset.o \
./src/main.o \
./src/trace_fifo.o \
./src/trace_udp.o 

C_DEPS += \
./src/iic_phyreset.d \
./src/main.d \
./src/trace_fifo.d \
./src/trace_udp.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"/home/ashton/repo/zynq_coresight/sources/sdk/free_ip_coresight_bsp/ps7_cortexa9_1/include" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


