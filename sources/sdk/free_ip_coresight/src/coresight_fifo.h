/*
 * coresight_fifo.h
 *
 *  Created on: May 12, 2019
 *      Author: ashton
 */

#ifndef SRC_CORESIGHT_FIFO_H_
#define SRC_CORESIGHT_FIFO_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

typedef struct {
	uint32_t * data_p;
	int32_t size;
} stream_pkt_t;

extern stream_pkt_t stream_pkt;
extern xQueueHandle xCoresightQueue;

int coresight_fifo_thread();

#endif /* SRC_CORESIGHT_FIFO_H_ */
