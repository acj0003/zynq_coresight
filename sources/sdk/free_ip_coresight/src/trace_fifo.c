/******************************************************************************
 *
 * Copyright (C) 2013 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/
/*****************************************************************************/
/**
 *
 * @file XLlFifo_polling_example.c
 * This file demonstrates how to use the Streaming fifo driver on the xilinx AXI
 * Streaming FIFO IP.The AXI4-Stream FIFO core allows memory mapped access to a
 * AXI-Stream interface. The core can be used to interface to AXI Streaming IPs
 * similar to the LogiCORE IP AXI Ethernet core, without having to use full DMA
 * solution.
 *
 * This is the polling example for the FIFO it assumes that at the
 * h/w level FIFO is connected in loopback.In these we write known amount of
 * data to the FIFO and Receive the data and compare with the data transmitted.
 *
 * Note: The TDEST Must be enabled in the H/W design inorder to
 * get correct RDR value.
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 3.00a adk 08/10/2013 initial release CR:727787
 * 5.1   ms  01/23/17   Modified xil_printf statement in main function to
 *                      ensure that "Successfully ran" and "Failed" strings
 *                      are available in all examples. This is a fix for
 *                      CR-965028.
 *       ms  04/05/17   Added tabspace for return statements in functions for
 *                      proper documentation and Modified Comment lines
 *                      to consider it as a documentation block while
 *                      generating doxygen.
 * </pre>
 *
 * ***************************************************************************
 */

/***************************** Include Files *********************************/
#include <stdio.h>
#include <string.h>

#include "xparameters.h"
#include "xil_exception.h"
#include "xstreamer.h"
#include "xil_cache.h"
#include "xllfifo.h"
#include "xstatus.h"
#include "coresight_fifo.h"
#include "xil_assert.h"

#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"

#ifdef XPAR_UARTNS550_0_BASEADDR
#include "xuartns550_l.h"       /* to use uartns550 */
#endif


/**************************** Type Definitions *******************************/

/***************** Macros (Inline Functions) Definitions *********************/

#define REMOTE_ADDR "192.168.1.206"

#define FIFO_DEV_ID	   	XPAR_AXI_FIFO_0_DEVICE_ID

#define WORD_SIZE 4			/* Size of words in bytes */

#define MAX_PACKET_LEN 4

#define NO_OF_PACKETS 64

#define MAX_DATA_BUFFER_SIZE 16384

#undef DEBUG

#define PUSH_STATS

/************************** Function Prototypes ******************************/
#ifdef XPAR_UARTNS550_0_BASEADDR
static void Uart550_Setup(void);
#endif

int XLlFifoPollingExample(XLlFifo *InstancePtr, u16 DeviceId);
int TxSend(XLlFifo *InstancePtr, u32 *SourceAddr);
int RxReceive(XLlFifo *InstancePtr, u32 *DestinationAddr);

/************************** Variable Definitions *****************************/
/*
 * Device instance definitions
 */
XLlFifo FifoInstance;

u32 SourceBuffer[MAX_DATA_BUFFER_SIZE * WORD_SIZE];
u32 DestinationBuffer[MAX_DATA_BUFFER_SIZE * WORD_SIZE];

extern TaskHandle_t udp_task_handle;

stream_pkt_t stream_pkt = { 0 };

/*****************************************************************************/
/**
 *
 * Main function
 *
 * This function is the main entry of the Axi FIFO Polling test.
 *
 * @param	None
 *
 * @return
 *		- XST_SUCCESS if tests pass
 * 		- XST_FAILURE if fails.
 *
 * @note		None
 *
 ******************************************************************************/
int coresight_fifo_thread() {
	int Status;

	xil_printf("--- Entering coresight_fifo_thread() ---\n\r");

	Status = XLlFifoPollingExample(&FifoInstance, FIFO_DEV_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Axi Streaming FIFO Polling Example Test Failed\n\r");
		xil_printf("--- Exiting main() ---\n\r");
		return XST_FAILURE;
	}

	xil_printf("Successfully ran Axi Streaming FIFO Polling Example\n\r");
	xil_printf("--- Exiting main() ---\n\r");

	vTaskDelete(0);
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
 *
 * This function demonstrates the usage AXI FIFO
 * It does the following:
 *       - Set up the output terminal if UART16550 is in the hardware build
 *       - Initialize the Axi FIFO Device.
 *	- Transmit the data
 *	- Receive the data from fifo
 *	- Compare the data
 *	- Return the result
 *
 * @param	InstancePtr is a pointer to the instance of the
 *		XLlFifo component.
 * @param	DeviceId is Device ID of the Axi Fifo Deive instance,
 *		typically XPAR_<AXI_FIFO_instance>_DEVICE_ID value from
 *		xparameters.h.
 *
 * @return
 *		-XST_SUCCESS to indicate success
 *		-XST_FAILURE to indicate failure
 *
 ******************************************************************************/
int XLlFifoPollingExample(XLlFifo *InstancePtr, u16 DeviceId) {
	XLlFifo_Config *Config;
	int Status;
	int i;
	int Error;
	Status = XST_SUCCESS;

	/* Initialize the Device Configuration Interface driver */
	Config = XLlFfio_LookupConfig(DeviceId);
	if (!Config) {
		xil_printf("No config found for %d\r\n", DeviceId);
		return XST_FAILURE;
	}

	/*
	 * This is where the virtual address would be used, this example
	 * uses physical address.
	 */
	Status = XLlFifo_CfgInitialize(InstancePtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed\n\r");
		return Status;
	}

	/* Check for the Reset value */
	Status = XLlFifo_Status(InstancePtr);
	XLlFifo_IntClear(InstancePtr, 0xffffffff);
	Status = XLlFifo_Status(InstancePtr);
	if (Status != 0x0) {
		xil_printf("\n ERROR : Reset value of ISR0 : 0x%x\t"
				"Expected : 0x0\n\r", XLlFifo_Status(InstancePtr));
		return XST_FAILURE;
	}

	/* Receive the Data Stream */
	Status = RxReceive(InstancePtr, DestinationBuffer);
	if (Status != XST_SUCCESS) {
		xil_printf("Receiving data failed");
		return XST_FAILURE;
	}

	return Status;
}

/*****************************************************************************/
/**
 *
 * RxReceive routine.It will receive the data from the FIFO.
 *
 * @param	InstancePtr is a pointer to the instance of the
 *		XLlFifo instance.
 *
 * @param	DestinationAddr is the address where to copy the received data.
 *
 * @return
 *		-XST_SUCCESS to indicate success
 *		-XST_FAILURE to indicate failure
 *
 * @note		None
 *
 ******************************************************************************/
int RxReceive(XLlFifo *InstancePtr, u32* DestinationAddr) {
	uint32_t high_water_mark = 0;
	int i;
	int Status;
	u32 RxWord;
	register u32 ReceiveLength;

	xil_printf(" Receiving data ....\n\r");
	BaseType_t XStatus;

	stream_pkt.data_p = DestinationAddr;
	uint32_t offset = 0;

	/* Start by clearing the size */

	int sock, new_sd;
	int size;

	struct sockaddr_in address, remote;

	memset(&address, 0, sizeof(address));

	if ((sock = lwip_socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		return (-1);

	address.sin_family = AF_INET;
	address.sin_port = htons(12345);
	address.sin_addr.s_addr = INADDR_ANY;

	if (lwip_bind(sock, (struct sockaddr *) &address, sizeof(address)) < 0)
		return (-1);

	BaseType_t xStatus = pdTRUE;
	struct sockaddr_in ra;

	memset(&ra, 0, sizeof(struct sockaddr_in));
	ra.sin_family = AF_INET;
	ra.sin_addr.s_addr = inet_addr(REMOTE_ADDR);
	ra.sin_port = htons(12345);

#ifdef PUSH_STATS
	int sock_stats, new_sd_stats;
	int size_stats;

	struct sockaddr_in address_stats, remote_stats;

	memset(&address_stats, 0, sizeof(address_stats));

	if ((sock_stats = lwip_socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		return (-1);

	address_stats.sin_family = AF_INET;
	address_stats.sin_port = htons(12350);
	address_stats.sin_addr.s_addr = INADDR_ANY;

	if (lwip_bind(sock, (struct sockaddr *) &address, sizeof(address)) < 0)
		return (-1);

	BaseType_t xStatus_stats = pdTRUE;
	struct sockaddr_in ra_stats;

	memset(&ra_stats, 0, sizeof(struct sockaddr_in));
	ra_stats.sin_family = AF_INET;
	ra_stats.sin_addr.s_addr = inet_addr(REMOTE_ADDR);
	ra_stats.sin_port = htons(12350);

#endif

	while (1) {
#ifdef FIFO_STORE_AND_FORWARD
		while (!XLlFifo_iRxOccupancy(InstancePtr)) {
			vTaskDelay(1);
		}
		/* Read Receive Length */
		ReceiveLength = (XLlFifo_iRxGetLen(InstancePtr)) / WORD_SIZE;

		if (ReceiveLength) {
			/* Start Receiving */

			stream_pkt.size = ReceiveLength;
			for (i = 0; i < ReceiveLength; i++) {

				uint32_t occu = XLlFifo_iRxOccupancy(InstancePtr);
				if (0x4000 <= occu) {
					xil_printf("overflow occurred\r\n");
				}
				high_water_mark =
				(occu > high_water_mark) ? occu : high_water_mark;
				/* if there is a byte in the fifo */
				if (occu) {
					RxWord = XLlFifo_RxGetWord(InstancePtr);
					*(DestinationAddr + i) = RxWord;
				}

			}
			/* Send over the entire packet */
			Status = XLlFifo_IsRxDone(InstancePtr);

			int status = lwip_sendto(sock, stream_pkt.data_p,
					sizeof(uint32_t) * stream_pkt.size, 0,
					(struct sockaddr*) &ra, sizeof(ra));
			if (0 == status) {
				xil_printf("transmit of data failed in %s\r\n", __FUNCTION__);
				close(sock);
				exit(3);
			}

		}
#else
		int num_read = 0;
		int partial = 0;
		uint32_t isr = 0;
		do {
			do {
				/* poll the length register until data is available */
				ReceiveLength = XLlFifo_iRxGetLen(InstancePtr);
				ReceiveLength &= 0x7FFFFFFF;
				ReceiveLength /= WORD_SIZE;
			} while (!ReceiveLength);

			/* Set the length of the UDP packet to the length of data about to be read */
			stream_pkt.size = ReceiveLength;

#ifdef PUSH_STATS
			char buffer[50];
			static u32 counter;
			counter += ReceiveLength;
			int n = sprintf(buffer, "%10lu\t%lu\r\n", counter, ReceiveLength);
			int status_stats = lwip_sendto(sock_stats, &buffer,
					sizeof(char) * n, 0, (struct sockaddr*) &ra_stats,
					sizeof(ra_stats));

			/* error check that it sent */
			if (0 == status_stats) {
				xil_printf("transmit of data failed in %s\r\n", __FUNCTION__);
				close(sock);
				exit(3);
			}
#endif
			/* Extract the new data from the FIFO */
			/* Pay attention to the for() loop conditions */
			for (int i = 0; i < ReceiveLength; i++, num_read++) {
				/*for (int i = 0; num_read < ReceiveLength; i++, num_read++) {*/
				/* get the word */
				RxWord = XLlFifo_RxGetWord(InstancePtr);
				/* store the word */
				*(DestinationAddr + i) = RxWord;

			}

			/* If there is data to transmit */

			/* transmit UDP packet */
			int status = lwip_sendto(sock, stream_pkt.data_p,
					sizeof(uint32_t) * stream_pkt.size, 0,
					(struct sockaddr*) &ra, sizeof(ra));

			/* error check that it sent */
			if (0 == status) {
				xil_printf("transmit of data failed in %s\r\n", __FUNCTION__);
				close(sock);
				exit(3);
			}

			/* See if a receive complete has been asserted */
			//Status = XLlFifo_IsRxDone(InstancePtr);
			/* If the message is indicated that it has completed, reset the sequence */
		} while (1);

#endif
	}

	return (XST_SUCCESS);
}

